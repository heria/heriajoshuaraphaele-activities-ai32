<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SongController;
use App\Http\Controllers\PlaylistController;
use App\Http\Controllers\PlaylistSongController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Songs
Route::get('/songs', [SongController::class, 'displaySongs']);
Route::post('/upload', [SongController::class, 'upload']);

//Playlist 
Route::get('/playlist', [PlaylistController::class, 'displayPlaylist']);
Route::post('/create', [PlaylistController::class, 'upload']);

// Playlist song
Route::get('/playlistsongs', [PlaylistSongController::class, 'displayPlaylistSong']);
Route::post('/create', [PlaylistSongController::class, 'upload']);
