<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\playlist;
use Illuminate\Support\Facades\DB;


class PlaylistController extends Controller
{
    public function displayPlaylist()
    {
        return DB::table('playlist')->get();
    }
    public function upload(Request $request)
    {

        $playlist = new playlist();
        $playlist->name = $request->name;
        $playlist->save();
        return $playlist;
    }
}
