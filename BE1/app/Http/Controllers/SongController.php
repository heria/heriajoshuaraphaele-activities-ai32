<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Song;
use Illuminate\Support\Facades\DB;

class SongController extends Controller
{
    public function displaySongs()
    {
        return DB::table('songs')->get();
    }
    public function upload(Request $request)
    {

        $song = new Song();
        $song->title = $request->title;
        $song->length = $request->length;
        $song->artist = $request->artist;
        $song->save();
        return $song;
    }
}
