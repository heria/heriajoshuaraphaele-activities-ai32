<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\playlist_songs;
use Illuminate\Support\Facades\DB;

class PlaylistSongController extends Controller
{
    public function displayPlaylistSong()
    {
        return DB::table('playlist_songs')->get();
    }
    public function upload(Request $request)
    {

        $song = new playlist_songs();
        $song->song_id = $request->song_id;
        $song->playlist_id = $request->playlist_id;
        $song->save();
        return $song;
    }
}
