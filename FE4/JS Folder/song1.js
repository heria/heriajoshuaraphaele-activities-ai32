//Aries
var mySong = document.getElementById("mySong");
var icon = document.getElementById("icon");

icon.onclick = function(){
  if(mySong.paused){
    mySong.play();
    icon.src="images/pauseBtn.png";
  }else{
    mySong.pause();
    icon.src = "images/playBtn.png";
  }
}
